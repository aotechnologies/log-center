# Log Center API Documentation

## Summary

Log Center API is a tool to centralize log output during app development

## Routes Description

### Create and app and get access token

#### Register an app

```rest
/apps/register
```

**input** JSON Object

```json
{
    "appName": "your-app-name",
    "passphrase": "your-passphrase"
}
```

**output** JSON Object

```json
{
    "appName": "your-app-name",
    "id": "your-app-id"
}
```

#### Get an access token

You need to get an access token to be authenticate your request.

```rest
/apps/get-token
```

**input** JSON Object

```json
{
    "passphrase": "mypassphrase",
    "appId": "your-app-id"
}
```

**output** JSON Object

```json
{
    "appName": "PharmacIvoire",
    "id": "5d95a9c42e046307b7d3e6c4",
    "token": "your-access-token"
}
```

The access token must be included in your request header for following routes

```rest
access-token: "your-access-token"
```

### Create log

#### Add log

```rest
/logs/add
```

**input** JSON Object

```json
{
    "logLevel": "info",
    "message": "My log",
    "additionalData": ""
}
```

Valid log levels are: ```ìnfo, debug, warn, error```

**output** JSON Object

```response
OK
```

### Query logs

#### Get app logs

```rest
/logs/
```

or

```rest
/apps/get/logs
```

**input** None

**output** JSON Object

```json
[
    {
        "appId": "your-app-id",
        "logLevel": "log-level",
        "message": "log-message",
        "additionalData": "",
        "createdAt": "creation-date"
    },
    ...
]
```

#### Query app logs

```rest
/logs/query
```

or

```rest
/apps/get/logs/query
```

**input** JSON Object

```json
{
    "logLevel": "log-level",
    "fromDate": "gte-date",
    "toDate": "lt-date"
}
```

All filter keys are optionals. If and empty object is sent, the API returns all app logs.

**output** JSON Object

```json
[
    {
        "appId": "your-app-id",
        "logLevel": "log-level",
        "message": "log-message",
        "additionalData": "",
        "createdAt": "creation-date"
    },
    ...
]
```
