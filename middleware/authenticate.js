const key = require('../key');
const jwt = require('jsonwebtoken');


/**
 * Authenticate route middleware
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @return {*}
 */
function authenticate(req, res, next) {
  const token = req.headers['access-token'];

  if (token) {
    jwt.verify(token, key.salt, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({message: 'Invalid access token'});
      } else {
        req.decodedToken = decodedToken;
        next();
      }
    });
  } else {
    return res.status(401).json({message: 'Missing access token'});
  }
}


module.exports = authenticate;
