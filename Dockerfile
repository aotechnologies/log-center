FROM node:12
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm i
RUN npm rebuild bcrypt --build-from-source
RUN npm audit fix --force

COPY . .

EXPOSE 8080
CMD ["npm", "start"]