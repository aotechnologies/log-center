const mongoose = require('mongoose');

const connectionString = 'mongodb://mongo:27017/log-center';

const connectDb = () => mongoose.connect(connectionString);

module.exports = connectDb;


