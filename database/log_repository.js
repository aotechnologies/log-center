const LogModel = require('../models/log');

/**
 * Create a log
 *
 * @param {*} data
 * @return {Promise}
 */
async function create(data) {
  const log = new LogModel(data);

  return await log.save();
}

/**
 * Returns all logs for a given app
 *
 * @param {String} appId
 * @return {Promise}
 */
async function getAppLogs(appId) {
  return LogModel.find({appId})
      .sort({createdAt: -1})
      .exec();
}

/**
 * Filter logs by a given query object
 *
 * @param {Object} filter
 * @return {Promise}
 */
async function query(filter) {
  // Validate query object
  Object.keys(filter).forEach((key) => {
    if (!['appId', 'logLevel', 'fromDate', 'toDate'].includes(key)) {
      throw Error('You must provide a valid filter object');
    }
  });

  // Ensure dates bounds are bath defined if provided
  if ((!filter.fromDate && filter.toDate) ||
    (filter.fromDate && !filter.toDate)) {
    throw Error('Invalid date bounds');
  }

  // Build date interval
  if (filter.fromDate && filter.toDate) {
    filter.createdAt = {$gte: filter.fromDate, $lt: filter.toDate};
    delete filter.fromDate;
    delete filter.toDate;
  }

  return LogModel.find(filter)
      .sort({createdAt: -1})
      .exec();
}

module.exports = {create, getAppLogs, query};
