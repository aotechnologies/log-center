'use strict';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const key = require('../key');

const AppModel = require('../models/app');
const logRepository = require('../database/log_repository');


/**
 * Create an app
 *
 * @param {Object} data
 * @return {Promise}
 */
async function create(data) {
  const app = new AppModel(data);

  return await app.save();
}

/**
 * Generate auth token
 *
 * @param {Object} data
 * @return {Promise}
 */
async function generateToken(data) {
  let appModel;

  return AppModel.findById(data.appId).exec()
      .then((app) => {
        if (app !== null) {
          appModel = app;
          return bcrypt.compare(data.passphrase, app.passphrase);
        }

        return Promise.reject(
            new Error('The provided appId does not match any app')
        );
      })
      .then((isValid) => {
        if (isValid) {
          const payload = {
            appName: appModel.appName,
            _id: appModel._id,
          };

          const token = jwt.sign(payload, key.salt, {
            expiresIn: '1d', // Expires in 24h
          });

          return {
            ...appModel.toJSON(),
            token,
          };
        }

        return Promise.reject(
            new Error('The passphrase is incorrect')
        );
      });
}

/**
 * Get an app matching given id
 *
 * @param {int} id
 * @return {Promise}
 */
function findById(id) {
  return AppModel.findById(id).exec();
}

/**
 * Get app logs
 * @param {*} filter
 * @return {Promise}
 */
function getLogs(filter) {
  return logRepository.query(filter);
}

module.exports = {create, find: findById, generateToken, getLogs};
