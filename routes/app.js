/* eslint-disable new-cap */
'use strict';

const router = require('express').Router();
const appRepository = require('../database/app_repository');
const authMiddleware = require('../middleware/authenticate');

router.post('/register', (req, res) => {
  appRepository.create(req.body)
      .then((app) => res.json(app))
      .catch((err) => res.status(400).send(err.message));
});

router.get('/get-token', (req, res) => {
  appRepository.generateToken(req.body)
      .then((token) => res.json(token))
      .catch((err) => res.status(400).json({message: err.message}));
});

router.use(authMiddleware).get('/get', (req, res) => {
  const appId = req.decodedToken._id;

  appRepository.find(appId)
      .then((app) => {
        return res.json(app);
      })
      .catch((err) => res.status(400).json(err));
});

router.use(authMiddleware).get('/get/logs', (req, res) => {
  const appId = req.decodedToken._id;

  appRepository.getLogs({appId})
      .then((logs) => {
        return res.json(logs);
      })
      .catch((err) => res.status(400).json({message: err.message}));
});

router.use(authMiddleware).post('/get/logs/query', (req, res) => {
  appRepository.getLogs({...req.body, appId: req.decodedToken._id})
      .then((logs) => {
        return res.json(logs);
      })
      .catch((err) => res.status(400).json({message: err.message}));
});

module.exports = router;
