/* eslint-disable new-cap */
const authMiddleware = require('../middleware/authenticate');
const logRepository = require('../database/log_repository');

const router = require('express').Router().use(authMiddleware);

router.get('/', (req, res) => {
  const appId = req.decodedToken._id;

  logRepository.getAppLogs(appId)
      .then((logs) => res.json(logs))
      .catch((err) => res.status(400).json({'message': err.message}));
});

router.post('/add', (req, res) => {
  logRepository.create(req.body)
      .then((_) => res.sendStatus(200))
      .catch((err) => res.status(400).json({'message': err.message}));
});

router.post('/query', (req, res) => {
  logRepository.query({...req.body, appId: req.decodedToken._id})
      .then((logs) => res.json(logs))
      .catch((err) => res.status(400).json({'message': err.message}));
});


module.exports = router;
