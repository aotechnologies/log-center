'use strict';

const express = require('express');
const connectDb = require('./database/connection');
const bodyParser = require('body-parser');

const appRoutes = require('./routes/app');
const logRoutes = require('./routes/logs');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();


app.use(bodyParser.json());

app.use('/apps', appRoutes);
app.use('/logs', logRoutes);

app.get('/', (req, res) => res.status(200).send('Welcome to Log Center'));

app.listen(PORT, HOST, () => {
  console.log(`Running on port ${PORT} ${HOST}`);

  connectDb().then(() => console.log('Db connected'));
});


module.exports = app;
