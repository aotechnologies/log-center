/* eslint-disable new-cap */
/* eslint-disable no-invalid-this */
'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const appSchema = mongoose.Schema({
  appName: {
    type: String,
    required: [true, 'You must provide an app name'],
    unique: [true, 'The app name must be unique'],
  },
  passphrase: {
    type: String,
    required: [true, 'You must provide a passphrase'],
  },
}, {
  timestamps: true,
  toJSON: {
    transform: (doc, ret) => {
      return {
        appName: ret.appName,
        id: ret._id,
      };
    },
  },
});

appSchema.pre('save', function(next) {
  const saltRounds = 10;
  bcrypt.genSalt(saltRounds)
      .then((salt) => {
        return bcrypt.hash(this.passphrase, salt);
      })
      .then((hash) => {
        this.passphrase = hash;

        next();
      });
});

module.exports = mongoose.model('App', appSchema);
