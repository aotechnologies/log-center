/* eslint-disable new-cap */

'use strict';

const mongoose = require('mongoose');

const logSchema = mongoose.Schema(
    {
      appId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'App',
      },
      logLevel: {
        type: String,
        required: [
          true,
          `You must specified a log level. 
          Possible values are "info", "warn", "error", "debug"`,
        ],
        lowercase: true,
        enum: ['info', 'error', 'warn', 'debug'],
      },
      message: {
        type: String,
        required: [true, 'You must specify a message'],
      },
      additionalData: {
        type: String,
        required: false,
      },
    },
    {
      timestamps: true,
      toJSON: {
        transform: (doc, ret) => {
          return {
            appId: ret.appId,
            logLevel: ret.logLevel,
            message: ret.message,
            additionalData: ret.additionalData,
            createdAt: ret.createdAt,
          };
        },
      },
    },
);

module.exports = mongoose.model('Log', logSchema);
